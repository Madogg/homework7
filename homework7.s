@ homework7.s
	.global _start

@ Prompt user to enter a character.
_start:
	MOV R7, #4
        MOV R0, #1
        MOV R2, #18
        LDR R1, =prompt
        SWI 0
@ Get input from keyboard.
_input:
	MOV R7, #3
	MOV R0, #0
	MOV R2, #2
	LDR R1, =char
	SWI 0
@ Get the character
_load:
	LDR R1, =char
	LDRB R2, [R1]
@ If character is a space
	CMP R2, #32
	BNE _number
@ Then
_space:
@   Display message "You entered a space"
	MOV R7, #4
	MOV R0, #1
	MOV R2, #22
	LDR R1, =spac
	SWI 0
@   Set return code to 110
	MOV R0, #110
        MOV R7, #1
        SWI 0
@ Else if character is an Ascii value for a number 0 through 9
_number:
	CMP R2, #'0'
	BLT _lower
	CMP R2, #'9'
	BGT _lower
@ Then 

@   Display message "You entered a number"
	MOV R7, #4
	MOV R0, #1
	MOV R2, #21
	LDR R1, =numb
	SWI 0
@   Set return code to 120
	MOV R0, #120
        MOV R7, #1
        SWI 0
@ Else if character is a lower case letter between a to z
_lower:
	CMP R2, #'a'
	BLT _upper
	CMP R2, #'z'
	BGT _upper
@ Then 

@   Display message "You entered a lower case letter"
	MOV R7, #4
        MOV R0, #1
        MOV R2, #32
        LDR R1, =low
        SWI 0
@   Set return code to 130
	MOV R0, #130
        MOV R7, #1
        SWI 0
@ Else if character is an upper case letter between A to Z
_upper:
	CMP R2, #'A'
	BLT _control
	CMP R2, #'Z'
	BGT _control
@ Then 

@   Display message "You entered an upper case letter"
	MOV R7, #4
        MOV R0, #1
        MOV R2, #33
        LDR R1, =up
        SWI 0
@   Set return code to 140
	MOV R0, #140
	MOV R7, #1
	SWI 0
@ Else if character is a control code
_control:
	CMP R2, #127
	BEQ _rubout
	CMP R2, #31
	BGT _punct
@ Then 
_rubout:
@   Display message "You entered a control code"
	MOV R7, #4
        MOV R0, #1
        MOV R2, #28
        LDR R1, =cc
        SWI 0
@   Set return code to 150
	MOV R0, #150
        MOV R7, #1
        SWI 0
@ Else assume you have a punctuation character
_punct:
@   Display message "You entered a punctuation character"
	MOV R7, #4
        MOV R0, #1
        MOV R2, #36
        LDR R1, =punk
        SWI 0
@   Set return code to 160
	MOV R0, #160
        MOV R7, #1
        SWI 0

.data
prompt:
.ascii "Enter a character\n"
char:
.ascii ""
spac:
.ascii "  You entered a space\n"
numb:
.ascii "You entered a number\n"
low:
.ascii "You entered a lower case letter\n"
up:
.ascii "You entered an upper case letter\n"
cc:
.ascii "You entered a controll code\n"
punk:
.ascii "You entered a punctuation character\n"
